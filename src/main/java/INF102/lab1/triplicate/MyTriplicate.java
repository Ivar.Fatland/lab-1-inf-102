package INF102.lab1.triplicate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyTriplicate<T> implements ITriplicate<T> {

    /**
     * Time complexity worst case: O(n)
     * @return The triplicate value, or null if triplicate not present.
     */
    @Override
    public T findTriplicate(List<T> list) {
        Map<T, Integer> counter = new HashMap<>();

        for (T item : list) {

            counter.put(
                    item,
                    counter.getOrDefault(
                            item,
                            0
                    ) + 1
            );

            if (counter.get(item).equals(3)) {
                return item;
            }

        }
        return null;
    }
    
}
